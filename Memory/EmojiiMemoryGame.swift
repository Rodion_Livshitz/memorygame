//
//  EmojiiMemoryGame.swift
//  Memory
//
//  Created by Rodion Livshitz on 30/08/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import SwiftUI

class EmojiMemoryGame {
    private var model: MemoryGame<String> = EmojiMemoryGame.createMemoryGame() // game itself
    
    static func createMemoryGame() -> MemoryGame<String> {
        let emojis: Array<String> = ["☠️", "👽", "🧕", "🌽", "🍏"]
        let numberOfPairsOfCards = Int.random(in: 2...emojis.count)
        return MemoryGame<String>(numberOfPairsOfCards: numberOfPairsOfCards) { index in
            return emojis[index]
        }
    }
    
    // MARK: - Access to the Model
    var cards: Array<MemoryGame<String>.Card> {
        return model.cards
    }
    
    // MARK: - Intent(s)
    func choose(card: MemoryGame<String>.Card) {
        return model.choose(card: card)
    }
}

