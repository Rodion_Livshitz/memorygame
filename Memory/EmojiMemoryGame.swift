//
//  EmojiMemoryGame.swift
//  Memory
//
//  Created by Rodion Livshitz on 30/08/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import SwiftUI

class EmojiMemoryGame: ObservableObject {
    var numberOfPairsOfCards: Int?
    let themes: Array<EmojiTheme> = [
            EmojiTheme(emojis: ["😀", "😇", "😎", "😱", "😷"], color:Color.orange, name: "Smiles"),
            EmojiTheme(emojis: ["🐶", "🐽", "🐔", "🐸", "🦄"], color:Color.blue, name: "Animals"),
            EmojiTheme(emojis: ["🍤", "🍳", "🥐", "🌽", "🍏"], color:Color.yellow, name: "Food"),
            EmojiTheme(emojis: ["⚽️", "🏈", "🏓", "⛸", "🥊"], color:Color.green, name: "Sport"),
            EmojiTheme(emojis: ["🇧🇪", "🇻🇬", "🇦🇲", "🇬🇪", "🇮🇱"], color:Color.red, name: "Flags"),
            EmojiTheme(emojis: ["📪", "📆", "📉", "🗳", "🎁"], color:Color.pink, name: "Shit")]
    
    // Published means each time when model is changed, objectWillChange.send(), will be called automaticly, and as a result, publish to the "world" that something is changed
    @Published var model: MemoryGame<String>? // game itself
    
    init(numberOfPairsOfCards: Int? = nil) {
        self.numberOfPairsOfCards = numberOfPairsOfCards
        resetGame()
        
    }
    
    // MARK: - Access to the Model
    var themeName: String {
        get { return themes[model!.id].name }
    }
    
    var themeColor: Color {
        get { return themes[model!.id].color }
    }
        
    var cards: Array<MemoryGame<String>.Card> {
        return model!.cards
    }
    
    var scores: Int {
        get { return model!.scores }
    }
    
    // MARK: - Intent(s)
    func choose(card: MemoryGame<String>.Card) {
        model!.choose(card: card)
    }
    
    func resetGame () {
        let choosenThemeIndex = Int.random(in: 0..<themes.count)
        let emojis: Array<String> = themes[choosenThemeIndex].emojis
        let numberOfPairsOfCards = self.numberOfPairsOfCards ?? Int.random(in: 2...emojis.count)
        model = MemoryGame<String>(id: choosenThemeIndex, numberOfPairsOfCards: numberOfPairsOfCards) { index in
            return emojis[index]
            
        }
    }
}


struct EmojiMemoryGame_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
