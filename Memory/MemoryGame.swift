//
//  MemoryGame.swift
//  Memory
//
//  Created by Rodion Livshitz on 30/08/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import Foundation

struct MemoryGame<CardContent> where CardContent: Equatable {
    private(set) var cards: Array<Card>
    private(set) var id: Int
    private(set) var scores: Int = 0
    private var indexOfTheOneAndOnlyFaceUpCard: Int? {
        get {
            cards.indices.filter { cards[$0].isFaceUp }.only
        }
        set {
            for index in cards.indices {
                self.cards[index].isFaceUp = index == newValue
            }
        }
    }
    
    mutating func choose(card: Card) {
        print("card choosen: \(card)")
        if let chosenIndex = self.cards.firstIndex(matching: card), !self.cards[chosenIndex].isFaceUp, !self.cards[chosenIndex].isMatched {
            
            if let potentionalMatch = indexOfTheOneAndOnlyFaceUpCard {
                
                if self.cards[potentionalMatch].content == self.cards[chosenIndex].content {
                    self.cards[potentionalMatch].isMatched = true
                    self.cards[chosenIndex].isMatched = true
                    self.scores += 2
                } else {
                    // Penalty for seen card(s)
                    if self.cards[chosenIndex].wasSeen {
                        self.scores -= 1;
                    }
                    
                    if self.cards[potentionalMatch].wasSeen {
                        self.scores -= 1;
                    }
                }
                
                self.cards[chosenIndex].isFaceUp = true
                self.cards[chosenIndex].wasSeen = true
                
            } else {
                indexOfTheOneAndOnlyFaceUpCard = chosenIndex
            }
            
            
        }
        
    }
    
    init(id: Int, numberOfPairsOfCards: Int, cardContentFactory: (Int) -> CardContent) {
        self.id = id
        self.cards = Array<Card>()
        for pairIndex in  0..<numberOfPairsOfCards {
            let content = cardContentFactory(pairIndex)
            self.cards.append(Card(content: content, id: pairIndex*2))
            self.cards.append(Card(content: content, id: pairIndex*2+1))
        }
        
        // randomize the cards
        self.cards.shuffle()
    }
    
    struct Card: Identifiable {
        var isFaceUpVar: Bool = false
        
        var isFaceUp: Bool {
            set {
                if newValue == false, isFaceUpVar == true {
                    wasSeen = true
                }
                isFaceUpVar = newValue
            }
            get {
                return isFaceUpVar
            }
        }
        var isMatched: Bool = false
        var wasSeen: Bool = false
        var content: CardContent
        var id: Int
    }
}
