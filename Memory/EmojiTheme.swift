//
//  EmojiTheme.swift
//  Memory
//
//  Created by Rodion Livshitz on 05/09/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import Foundation
import SwiftUI

struct EmojiTheme {
    var emojis: Array<String>
    var color: Color
    var name: String
}
