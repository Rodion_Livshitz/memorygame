//
//  Array+Identifiable.swift
//  Memory
//
//  Created by Rodion Livshitz on 04/09/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import Foundation

extension Array where Element: Identifiable {
    func firstIndex(matching: Element) -> Int? {
        for index in 0..<self.count {
            if self[index].id == matching.id {
                return index
            }
        }
        return nil
    }
}

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
