//
//  Cardify.swift
//  Memory
//
//  Created by Rodion Livshitz on 23/09/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import SwiftUI


struct Cardify : ViewModifier {
    var isFaceUp : Bool
    func body(content: Content) -> some View {
        ZStack {
            if (isFaceUp) {
                RoundedRectangle(cornerRadius: cornerRadius).fill(Color.white)
                RoundedRectangle(cornerRadius: cornerRadius).stroke(lineWidth: endLineWidth)
                content
            } else {
                RoundedRectangle(cornerRadius: cornerRadius).fill()
            }
        }
    }
        
        
    private let cornerRadius: CGFloat = 10.0
    private let endLineWidth: CGFloat = 3.0

}

extension View {
    func cardify(isFaceUp: Bool) -> some View {
        self.modifier(Cardify(isFaceUp: isFaceUp))
    }
}
