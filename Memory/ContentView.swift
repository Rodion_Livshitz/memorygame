//
//  ContentView.swift
//  Memory
//
//  Created by Rodion Livshitz on 30/08/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    var viewModel: EmojiiMemoryGame
    var body: some View {
        HStack {
            ForEach(viewModel.cards) {card in
                CardView(card: card)
            }
        }
        .padding()
        .foregroundColor(Color.orange)
        .font((viewModel.cards.count == 10) ? Font.title : Font.largeTitle)
    }
}

struct CardView: View {
    var card: MemoryGame<String>.Card
    
    var body: some View {
        ZStack {
            if (card.isFaceUp) {
                RoundedRectangle(cornerRadius: 10.0).fill(Color.white)
                RoundedRectangle(cornerRadius: 10.0).stroke(lineWidth: 3.0)
                Text(card.content)
            } else {
                RoundedRectangle(cornerRadius: 10.0).fill()
            }
        }
        .aspectRatio(2.0/3.0, contentMode: .fit)
    }
}







struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiMemoryGameView(viewModel: EmojiiMemoryGame())
    }
}
