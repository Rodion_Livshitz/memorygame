//
//  ContentView.swift
//  Memory
//
//  Created by Rodion Livshitz on 30/08/2020.
//  Copyright © 2020 Rodion Livshitz. All rights reserved.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    // @ObservedObject affective UI (draw each time when viewModel is changed)
    @ObservedObject var viewModel: EmojiMemoryGame
    
    var body: some View {
        VStack {
            Grid(viewModel.cards) { card in
                CardView(card: card).onTapGesture {
                    self.viewModel.choose(card: card)
                }
                .padding()
            }
            ControlPanelView(viewModel: viewModel)
        }
        .padding()
        .foregroundColor(viewModel.themeColor)
    }
}

struct ControlPanelView: View {
    @ObservedObject var viewModel: EmojiMemoryGame
    
    var body: some View {
        HStack {
            Button("Reset") {
                self.viewModel.resetGame()
            }
            Text("Scores: \(self.viewModel.scores)")
            Text(self.viewModel.themeName)
        }
    }
}

struct CardView: View {
    var card: MemoryGame<String>.Card
    
    @ViewBuilder
    var body: some View {
        GeometryReader { geometry in
            if card.isFaceUp || !card.isMatched {
                ZStack {
                    Pie(startAngle: Angle.degrees(0-90), endAngle: Angle.degrees(110-90), clockwise: true).padding(15).opacity(0.4)
                        Text(card.content).font(Font.system(size: fontSize(for: geometry.size)))
                }
                .cardify(isFaceUp: card.isFaceUp)
            }
            
        }
        
        .aspectRatio(2.0/3.0, contentMode: .fit)
    }
    
    // MARK: - Drawing Constants
    func fontSize(for size: CGSize) -> CGFloat {
        min(size.width, size.height) * 0.68
    }
}







struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let game = EmojiMemoryGame(numberOfPairsOfCards: 1)
        game.choose(card: game.cards[0])
        return EmojiMemoryGameView(viewModel: game)
    }
}
